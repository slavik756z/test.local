<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 14.04.19
 * Time: 21:44
 */

namespace App\Repository;


use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class ProductRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $registry;

    public function __construct(EntityManagerInterface $registry)
    {
        $this->registry = $registry;
    }

    public function updateStatus(): void
    {
        $this->registry
            ->createQueryBuilder()
            ->update(Product::class, 'p')
            ->set('p.status', ':statusPublish')
            ->where("p.status = :status")
            ->setParameter(':status', Product::STATUS_PENDING)
            ->setParameter(':statusPublish', Product::STATUS_PUBLISH)
            ->getQuery()
            ->execute();
    }
}