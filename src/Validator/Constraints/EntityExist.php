<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 14:51
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class EntityExist
 * @package App\Validator\Constraints
 *
 * @Annotation
 */
class EntityExist extends Constraint
{
    public $message = 'Entity with Id = {{ id }} dosn\'t exist';
}