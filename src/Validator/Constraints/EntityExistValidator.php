<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 14:59
 */

namespace App\Validator\Constraints;


use App\Entity\Category;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class EntityExistValidator extends ConstraintValidator
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof EntityExist) {
            throw new UnexpectedTypeException($constraint, EntityExist::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_array($value)) {

            throw new UnexpectedValueException($value, 'string');
        }

        foreach ($value as $item) {
            if(!$this->registry->getRepository(Category::class)->findOneBy(['id' => $item]) instanceof Category) { // TODO - Can be move to repository service

                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ id }}', $item)
                    ->addViolation();
            }
        }
    }
}