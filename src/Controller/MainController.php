<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 10:32
 */

namespace App\Controller;


use App\Service\ImageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MainController extends AbstractController
{
    /**
     * @Route("api/upload-image", methods={"POST"})
     *
     * @param Request $request
     * @param ImageService $imageService
     * @return JsonResponse
     */
    public function uploadImageAction(Request $request, ImageService $imageService): JsonResponse
    {
        // TODO - should be add listener for "api/" route patten which will be handle exception
        return new JsonResponse($imageService->saveImage($request->files->get('image','')));
    }
}