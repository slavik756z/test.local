<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 14.04.19
 * Time: 18:08
 */

namespace App\Service;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    /**
     * @var string
     */
    private $rootDir;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function saveImage(UploadedFile $image): string
    {
        // - TODO is not finished need validation.
        // - TODO also need a system which will be keep all files in db.And we should relate files entities with other type.

        $newFile = $image->move($this->rootDir . '/public/images/', uniqid('img-') . '.' . $image->guessExtension());

        return 'images/' . $newFile->getFilename();
    }
}