<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 11:12
 */

namespace App\Dto\Product;


use App\Validator\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class ProductInput
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $title;

    /**
     * @var string
     *
     * @Assert\Type("string")
     */
    public $status;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}."
     *     )
     */
    public $price;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     */
    public $discount;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $link;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $location;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @Assert\Type("\DateTime")
     */
    public $activeUntil;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $delivery;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $description;

    /**
     * @var integer[]
     *
     * @EntityExist()
     */
    public $categories;

    /**
     * @var string
     *
     * @Assert\Type("string")
     *
     * TODO - also can add custom constraint on existing file
     */
    public $imagePath;
}