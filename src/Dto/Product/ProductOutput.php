<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 11:12
 */

namespace App\Dto\Product;


use App\Entity\Category;

class ProductOutput
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $status;

    /**
     * @var float
     */
    public $price;

    /**
     * @var float
     */
    public $discount;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $location;

    /**
     * @var \DateTime
     */
    public $activeUntil;

    /**
     * @var string
     */
    public $delivery;

    /**
     * @var string
     */
    public $description;

    /**
     * @var Category[]
     */
    public $categories;

    /**
     * @var string
     */
    public $imagePath;
}