<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 10.04.19
 * Time: 17:15
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Dto\Product\ProductInput;
use App\Dto\Product\ProductOutput;
//use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Class Product
 * @package App\Entity
 *
 * @ApiResource(
 *     input=ProductInput::class,
 *     output=ProductOutput::class,
 *     itemOperations={
 *         "put",
 *         "get",
 *         "delete"={"access_control"="is_granted('ROLE_ADMIN')", "access_control_message"="Sorry, but you are not the book owner."}
 *     },
 *     collectionOperations={"get","post"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"email"})
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_PUBLISH = 'publish';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     */
    protected $email;
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    protected $price;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float")
     */
    protected $discount;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string")
     */
    protected $link;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string")
     */
    protected $location;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="active_until", type="datetime")
     */
    protected $activeUntil;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery", type="string")
     */
    protected $delivery;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="Category", cascade={"persist"})
     * @ORM\JoinTable(name="product_category",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *     )
     */
    protected $categories = [];

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", nullable=true)
     */
    protected $imagePath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at",type="datetime")
     */
    protected $createdAt;

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount(float $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return \DateTime
     */
    public function getActiveUntil(): \DateTime
    {
        return $this->activeUntil;
    }

    /**
     * @param \DateTime $activeUntil
     */
    public function setActiveUntil(\DateTime $activeUntil): void
    {
        $this->activeUntil = $activeUntil;
    }

    /**
     * @return string
     */
    public function getDelivery(): string
    {
        return $this->delivery;
    }

    /**
     * @param string $delivery
     */
    public function setDelivery(string $delivery): void
    {
        $this->delivery = $delivery;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category $category
     */
    public function addCategories(Category $category): void
    {
        $this->categories[] = $category;
    }

    /**
     * @return string
     */
    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     */
    public function setImagePath(?string $imagePath): void
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @ORM\PrePersist()
     * @throws \Exception
     */
    public function setCreatedAt():void
    {
      $this->createdAt = new \DateTime();
    }
}