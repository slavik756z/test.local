<?php

namespace App\Command;

use App\Repository\ProductRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ChangeProductsStatusCommand extends Command
{
    const MODE_MULTIPLE = 'multiple';
    const MODE_SINGLE = 'single';

    protected static $defaultName = 'app:change-products-status';

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(?string $name = null,ProductRepository $productRepository)
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Process products statuses')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('Start processing');

        $this->productRepository->updateStatus();

        $io->success('All updated!');
    }
}
