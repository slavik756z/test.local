<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 11:23
 */

namespace App\DataTransformer\Product;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Product\ProductOutput;
use App\Entity\Product;

class ProductOutputDataTransformer implements DataTransformerInterface
{
    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param object|Product $object
     *
     * @return object|ProductOutput
     */
    public function transform($object, string $to, array $context = [])
    {
        $productOutput = new ProductOutput();
        $productOutput->title = $object->getTitle();
        $productOutput->status = $object->getStatus();

        $productOutput->price = $object->getPrice();
        $productOutput->discount = $object->getDiscount();
        $productOutput->delivery = $object->getDelivery();
        $productOutput->activeUntil = $object->getActiveUntil();
        $productOutput->imagePath = $object->getImagePath();
        $productOutput->description = $object->getDescription();
        $productOutput->link = $object->getLink();

        $productOutput->categories = $object->getCategories();
        $productOutput->location = $object->getLocation();

        return $productOutput;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        return true;
    }
}