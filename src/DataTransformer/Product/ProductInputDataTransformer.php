<?php
/**
 * Created by PhpStorm.
 * User: vyacheslav
 * Date: 11.04.19
 * Time: 11:18
 */

namespace App\DataTransformer\Product;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Product\ProductInput;
use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductInputDataTransformer implements DataTransformerInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var ValidatorInterface
     */
    private $validation;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        ValidatorInterface $validation,
        ManagerRegistry $registry
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->validation = $validation;
        $this->registry = $registry;
    }

    /**
     * Transforms the given object to something else, usually another object.
     * This must return the original object if no transformation has been done.
     *
     * @param object|ProductInput $object
     *
     * @return object
     */
    public function transform($object, string $to, array $context = [])
    {
        $error = $this->validation->validate($object);

        if (count($error) > 0) {
            throw new InvalidParameterException($error->get(0)->getMessage());
        }

        $product = new Product();
        $product->setTitle($object->title);
        $product->setEmail($object->email); // TODO - on PUT disable update
        $product->setStatus(Product::STATUS_PENDING);
        $product->setPrice($object->price);
        $product->setDiscount($object->discount);
        $product->setActiveUntil($object->activeUntil);
        $product->setLocation($object->location);
        $product->setDescription($object->description);
        $product->setDelivery($object->delivery);
        $product->setImagePath($object->imagePath);
        $product->setLink($object->link);

        if (!empty($object->categories)) {
            foreach ($object->categories as $categoryId) {
                $product->addCategories(
                    $this->registry->getRepository(Category::class)->findOneBy(
                        ['id' => $categoryId]
                    ) // TODO - Can be move to repository service
                );
            }
        }

        if (Product::STATUS_PUBLISH === $object->status && $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $product->setStatus($object->status);
        }

        return $product;
    }

    /**
     * Checks whether the transformation is supported for a given object and context.
     *
     * @param object $object
     * @return bool
     */
    public function supportsTransformation($object, string $to, array $context = []): bool
    {
        if ($object instanceof Product) {
            return false;
        }

        return true;
    }
}